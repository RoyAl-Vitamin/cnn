import os
import tensorflow as tf
import numpy as np
from datetime import datetime
import dataset as kavdataset
import gc

class Train:
    
    train_sample_count = 0
    
    def __init__(self, cnn, inputNN, out, train_rate = 0.4, batchSize=32):
        self.cnn = cnn
        self.batchSize = batchSize
        # labeled results for input
        categoryCount = cnn.resultCount
        self.train_labels = tf.placeholder(tf.float32, shape=[None,categoryCount])
        self.input = inputNN
        self.out = out
        self.loss = self.loss(self.train_labels, out)
        self.train = self.train(train_rate)
        self.accuracy = cnn.accuracy(self.train_labels, out)
    
    def loss(self, labels, out):
        entropy = tf.nn.softmax_cross_entropy_with_logits(labels=labels, logits=out)
        return tf.reduce_sum(entropy)
    
    def train(self, train_rate = 0.4):
        return tf.train.AdagradOptimizer(train_rate).minimize(self.loss)
    
    def setLogDir(self, path):
        self.logPath = path
    
#     def loadFiles(self, items):
#         fnames = self.extractFileNames(items)
#         files = []
#         for fname in fnames:
#             img = tf.read_file(fname)
#             img_decoded = tf.image.decode_jpeg(img)
#             image_resized = tf.image.resize_images(img_decoded, 
#                                                    [self.cnn.input_width, self.cnn.input_height])
#             files.append(image_resized)
#             
#         return tf.convert_to_tensor(np.asarray(files))
# 
#     def loadFilesByFileNames(self, fnames):
#         files = []
#         for fname in fnames:
#             img = tf.read_file(fname)
#             img_decoded = tf.image.decode_jpeg(img)
#             image_resized = tf.image.resize_images(img_decoded, 
#                                                    [self.cnn.input_width, self.cnn.input_height])
#             files.append(image_resized)
#             
#         return files

    def loadDataFromCatalog(self, pattern, category):
        def _parse_function(filename):
            return filename, tf.one_hot(category-1,categoryCount)
                   
        categoryCount = int(self.out.shape[1])
        ds = tf.data.Dataset.list_files(pattern).map(_parse_function)
        if not hasattr(self,"dataset"):
            self.dataset = ds
        else:
            self.dataset = self.dataset.concatenate(ds)
               
        count_files = len(os.listdir(os.path.dirname(pattern)))
        self.train_sample_count+=count_files     
        print("scheduled to load ",count_files,"samples")
              
    def prepare(self,session, validatePercent = 0.2):
        print("prepare training")
        dataset = self.dataset.shuffle(self.train_sample_count)
        #dataset = self.dataset
        # split dataset
        validCount = int(validatePercent*self.train_sample_count)
        self.valid_dataset = dataset.take(validCount)
        valid_dataset_size = validCount
        self.train_dataset = dataset.skip(validCount)
        train_dataset_size = self.train_sample_count - validCount
        # align datasets
        self.valid_dataset, addcount = self.align_dataset(self.valid_dataset,valid_dataset_size)
        self.train_sample_count+=addcount
        valid_dataset_size+=addcount
        
        self.train_dataset, addcount = self.align_dataset(self.train_dataset,train_dataset_size)
        self.train_sample_count+=addcount
        train_dataset_size+=addcount
        
        self.train_dataset_size = train_dataset_size
        self.valid_dataset_size = valid_dataset_size
        print("Total",self.train_sample_count,"samples")
        print("Validate dataset include",valid_dataset_size,"samples (",valid_dataset_size*100*1.0/self.train_sample_count,"%)")
        print("Train dataset include",train_dataset_size,"samples")
        
    def align_dataset(self, dataset, count):
        addcount = self.batchSize-count % self.batchSize
        if addcount>0:
            dataset = dataset.concatenate(dataset.take(addcount))
        return dataset, addcount
    
    def extractLabels(self, items):
        arr = []
        for itm in items:
            arr.append(itm['label'])
        return tf.convert_to_tensor(np.asarray(arr))

    def extractFileNames(self, items):
        arr = []
        for itm in items:
            arr.append(itm['fn'])
        return arr
    
    def _convToMyDataSet(self,ds,session):
        iterator = ds.make_one_shot_iterator()
        train_array = self.train_dataset_size*[None]
        i = 0
        while True:
            try:
                fname, label = session.run(iterator.get_next())
                train_array[i] = {"fn":fname, "label":label}
                i+=1
                if i % 100 == 0:
                    print("conv count =",i)
            except tf.errors.OutOfRangeError:
                break
        print("finish conv (",i, ")")
        return kavdataset.Dataset(train_array)
        
    def run(self,session, epochCount):
        print("run training...")
#         train_dataset = self._convToMyDataSet(self.train_dataset, session)
#         
#         self.train_loss_array = []
#         self.train_accuracy_array = []
#         for i in range(epochCount):
#             print("    epoch=",i+1, "...")
#             train_dataset.suffle()
#             iterator = train_dataset.make_one_shot_iterator(self.batchSize)
#             j=0
#             c=0
#             while True:
#                 item = iterator.get_next()
#                 if item is None:
#                     break
#                 label = self.extractLabels(item)
#                 sample = self.loadFiles(item)
#                                         
#                 c += sample.shape[0]
#                 session.run([self.train], feed_dict={self.input:sample, self.train_labels:label, 
#                                                          self.cnn.train_mode:True})
#                 j+=1
#                 print('            step %d: %.2f sapmles' % (j+1,c))
#             print('        calc test lost and accuracy')
#             testds = self.train_dataset.batch(4*self.batchSize)
#             test_batch = testds.make_one_shot_iterator().get_next()
#             sample, label = session.run(test_batch)
#             loss, accuracy = session.run([self.loss,self.accuracy],feed_dict={self.input:sample, self.train_labels:label,
#                                                                               self.cnn.train_mode:False})
#             self.train_loss_array.append(loss)
#             self.train_accuracy_array.append(accuracy)
#             print('        epoche %d:' % (i+1), ' loss =', self.train_loss_array,' accuracy = ', self.train_accuracy_array)
#         print('calc validate lost and accuracy')
#         valid_batch = self.valid_dataset.batch(4*self.batchSize).make_one_shot_iterator().get_next()
#         sample, label = session.run(valid_batch)
#         loss, accuracy = session.run([self.loss,self.accuracy],feed_dict={self.input:sample, self.train_labels:label,
#                                                                           self.cnn.train_mode:False})
#         print("loss = ",loss)
#         print("accuracy = ",accuracy)

class MNISTTrain(Train):
    
    def loadFromMNIST(self, datasets):
        self.dataset = datasets

    def prepare(self,session, validatePercent = 0.2):
        return 0
        
    def run(self,session, epochCount):
        print("run training...")
        
        self.train_loss_array = []
        self.train_accuracy_array = []
        for i in range(epochCount):
            sample, label = self.dataset.train.next_batch(self.batchSize)
            session.run(self.train,feed_dict={self.input:sample, self.train_labels:label, 
                                              self.cnn.train_mode:True})
            if i%100 == 0:
                loss, accuracy = session.run([self.loss,self.accuracy], 
                                             feed_dict={self.input:sample, self.train_labels:label, 
                                                        self.cnn.train_mode:False})
                print('        step %d: loss = %.2f accuracy = %.2f' % (i+1, loss, accuracy))
                self.train_loss_array.append(loss)
                self.train_accuracy_array.append(accuracy)
        print("result:")
        print("    loss process =",self.train_loss_array)
        print("    accuracy process=",self.train_accuracy_array)
        accuracy = session.run(self.accuracy, 
                                     feed_dict={self.input:self.dataset.test.images, self.train_labels:self.dataset.test.labels, 
                                                self.cnn.train_mode:False})
        print("    Final accuracy = %.2f" % (accuracy))
    
class TrainEx(Train):

    def run(self,session, epochCount):

        def _loadfile(fname):
            img = tf.read_file(fname)
            img_decoded = tf.image.decode_jpeg(img)
            return tf.image.resize_images(img_decoded, 
                            [self.cnn.input_width, self.cnn.input_height])

        def _load_bach_file(fnames, labels):
            bach_file_op = tf.map_fn(_loadfile,fnames,dtype=tf.float32)
            return bach_file_op, labels 
        
                            
        print("run training...")

        train_dataset = self.train_dataset.batch(self.batchSize)
        #load files
        train_dataset = train_dataset.map(_load_bach_file)

        
        writer = tf.summary.FileWriter(self.logPath+"\\train", session.graph)
        summaryloss = tf.summary.scalar('loss', self.loss)
        
        self.train_loss_array = []
        self.train_accuracy_array = []
        for i in range(epochCount):
            print("    epoch=",i+1, "...")
            start = datetime.now()
            iterator = train_dataset.make_one_shot_iterator()
            j=0
            c=0
            while True:
                try:
                    sample, label = session.run(iterator.get_next())
                    c += self.batchSize
                    session.run([self.train], feed_dict={self.input:sample, self.train_labels:label, 
                                                                             self.cnn.train_mode:True})
                    j+=1
                    print('            step %d: %.2f sapmles' % (j+1,c))
                except tf.errors.OutOfRangeError:
                    break
            duration = datetime.now()-start
            print('        calc test lost and accuracy')
            testds = self.train_dataset.batch(2*self.batchSize)
            testds = testds.map(_load_bach_file)
            test_batch = testds.make_one_shot_iterator().get_next()
            sample, label = session.run(test_batch)
            loss, accuracy, loss_summary_result = session.run([self.loss,self.accuracy, summaryloss],feed_dict={self.input:sample, self.train_labels:label,
                                                                              self.cnn.train_mode:False})
            writer.add_summary(loss_summary_result,global_step=i)
            self.train_loss_array.append(loss)
            self.train_accuracy_array.append(accuracy)
            print('        epoche %d:' % (i+1), ' loss =', loss,' accuracy = ', accuracy,' duration = ', duration)
            print('        history:  loss =', self.train_loss_array,' accuracy = ', self.train_accuracy_array)
            gc.collect()
        print('calc validate lost and accuracy')
        validds = self.valid_dataset.batch(2*self.batchSize)
        validds = validds.map(_load_bach_file)
        valid_batch = validds.make_one_shot_iterator().get_next()
        sample, label = session.run(valid_batch)
        loss, accuracy = session.run([self.loss,self.accuracy],feed_dict={self.input:sample, self.train_labels:label,
                                                                          self.cnn.train_mode:False})
        print("loss = ",loss)
        print("accuracy = ",accuracy)
