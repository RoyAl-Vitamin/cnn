import numpy as np
import tensorflow as tf
from tensorflow.python.ops import gen_io_ops

class Dataset:

    def __init__(self, data):
        self.data = data
    
    @staticmethod
    def list_files(file_pattern):
        return Dataset(gen_io_ops.matching_files(file_pattern))
    
    def append(self, data):
        self.data = tf.concat([self.data,data], 0)
    
    def map(self, func, dtype=None):
        return Dataset(tf.map_fn(func,self.data, dtype=dtype))

    def suffle(self):
        np.random.shuffle(self.data)
        
    def make_one_shot_iterator(self,batch_size):
        return BatchIterator(self.data,batch_size)
    
class BatchIterator:

    def __init__(self, data,batch_size):
        self.data = np.asarray(np.array_split(data,len(data)/batch_size))
        self.pos = 0
        self.batchSize = batch_size
    
    def get_next(self):
        self.pos+=1
        if self.pos>len(self.data):
            return None 
        return self.data[self.pos-1]