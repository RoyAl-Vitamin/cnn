import tensorflow as tf
import numpy as np
import matplotlib.image as mpimg
    
import ru.pstu.vmm.kav.nn as nn
import ru.pstu.vmm.kav.learning as learning
from tensorflow.examples.tutorials.mnist import input_data

if __name__ == '__main__':
    
    TRAIN_PATH = "F:\\WORK\\SCIENCE\\DiserDoctor-Calc\\TenzorFlow\\data\\mnist\\"
    
    print("start")    
    
    TRAIN_MODE = True
    # epoche count
    TRAIN_EPOCHE_COUNT = 20000
    # size of image batch
    BATCH_SIZE = 100
    # size of image
    imageWidth = 28
    imageHeight = 28
    # count category of results
    CATEGORY_COUNT = 10
    # count channel in input images (RGB)
    CHANNEL_COUNT = 1


    # input for MNIST Lecun
    inputNN = tf.placeholder(tf.float32, [None,784])
    #inputNN = tf.placeholder(tf.float32, shape=[BATCH_SIZE,imageWidth,imageHeight,CHANNEL_COUNT])
    x_image = tf.reshape(inputNN, [-1,28,28,1])
    
    cnn = nn.CNN(tf, imageWidth,imageHeight,input_channel = CHANNEL_COUNT)
    l1_out = cnn.layer1(x_image)
    l2_out = cnn.layer2(l1_out)
    l3_out = cnn.layer(3, l2_out,feature_count=128)
    fc1_out = cnn.fullyconn(l3_out, 700)
    drop_out = cnn.dropout(fc1_out)
    #output
    output = cnn.out(drop_out, CATEGORY_COUNT)
    
    print(output)
    
    if TRAIN_MODE:
        train = learning.MNISTTrain(cnn, inputNN, output, train_rate=1e-4, batchSize=BATCH_SIZE)
        train.loadFromMNIST(input_data.read_data_sets(TRAIN_PATH, one_hot=True))
        
        loss = train.loss
        accuracy = train.accuracy
    
    
    tf.summary.FileWriter("F:\WORK\SCIENCE\DiserDoctor-Calc\TenzorFlow\log",tf.get_default_graph())
    
    with tf.Session() as sess:
        init = (tf.global_variables_initializer(), tf.local_variables_initializer())
        sess.run(init)
        if TRAIN_MODE:
            train.run(sess,TRAIN_EPOCHE_COUNT)
    
    print("finish")    
