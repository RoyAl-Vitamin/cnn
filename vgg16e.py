import tensorflow as tf
    
import nn as nn
import learning as learning

if __name__ == '__main__':
    
#     TRAIN_PATH = "F:\\WORK\\SCIENCE\\DiserDoctor-Calc\\TenzorFlow\\data\\flowers\\"
#    TRAIN_PATH = "c:\\temp\\flowers\\"
#    TRAIN_PATH = sys.argv[1]
    TRAIN_PATH = "/flowers/"
#    LOG_PATH = "F:\\WORK\\SCIENCE\\DiserDoctor-Calc\\TenzorFlow\\log" 
#    LOG_PATH = os.getenv('TEST_TMPDIR', '/tmp')
    LOG_PATH = "/log"
    
    print("start")    
    
    TRAIN_MODE = True
    
    TRAIN_RATE = 0.4
    # epoche count
    TRAIN_EPOCHE_COUNT = 100
    # size of image batch
    BATCH_SIZE = 32
    # size of image
    imageWidth = 224
    imageHeight = 224
    # count category of results
    CATEGORY_COUNT = 5
    # count channel in input images (RGB)
    CHANNEL_COUNT = 3

    # test image
    #image = mpimg.imread(TRAIN_PATH+"flowers\\sample1.png")
    #print("load file succ image.shape = ", image.shape)

    print("TRAIN_PATH = ", TRAIN_PATH)

    # input
    inputNN = tf.placeholder(tf.float32, shape=[None,imageWidth,imageHeight,CHANNEL_COUNT])
    
    cnn = nn.CNN(tf, imageWidth,imageHeight,input_channel = CHANNEL_COUNT)
    l1_out = cnn.layerConv(1,inputNN,64,3)
    l2_out = cnn.layerConv(2,l1_out,64,3)
    l3_out = cnn.layerPool(3,l2_out)
    l4_out = cnn.layerConv(4,l3_out,128,3)
    l5_out = cnn.layerConv(5,l4_out,128,3)
    l6_out = cnn.layerPool(6,l5_out)
    l7_out = cnn.layerConv(7,l6_out,256,3)
    l8_out = cnn.layerConv(8,l7_out,256,3)
    l9_out = cnn.layerConv(9,l8_out,256,3)
    l10_out = cnn.layerConv(10,l9_out,256,3)
    l11_out = cnn.layerPool(11,l10_out)
    l12_out = cnn.layerConv(12,l11_out,512,3)
    l13_out = cnn.layerConv(13,l12_out,512,3)
    l14_out = cnn.layerConv(14,l13_out,512,3)
    l15_out = cnn.layerConv(15,l14_out,512,3)
    l16_out = cnn.layerPool(16,l15_out)
    l17_out = cnn.layerConv(17,l16_out,512,3)
    l18_out = cnn.layerConv(18,l17_out,512,3)
    l19_out = cnn.layerConv(19,l18_out,512,3)
    l20_out = cnn.layerConv(20,l19_out,512,3)
    l21_out = cnn.layerPool(21,l20_out)
    fc1_out = cnn.fullyconn(l21_out, 4096)
    fc2_out = cnn.dense(2,fc1_out, 4096)
    drop_out = cnn.dropout(fc2_out, 0.3)
    fc3_out = cnn.dense(3,drop_out, 4096)
    #output
    output = cnn.out(fc3_out, CATEGORY_COUNT)
    
    print(output)
    
    if TRAIN_MODE:
        train = learning.TrainEx(cnn, inputNN, output, batchSize=BATCH_SIZE, train_rate = TRAIN_RATE)
        #train.loadDataFromCatalog(TRAIN_PATH+"!sample\\*.jpg", 1)
        train.loadDataFromCatalog(TRAIN_PATH+"daisy/*.jpg", 1)
        train.loadDataFromCatalog(TRAIN_PATH+"dandelion/*.jpg", 2)
        train.loadDataFromCatalog(TRAIN_PATH+"rose/*.jpg", 3)
        train.loadDataFromCatalog(TRAIN_PATH+"sunflower/*.jpg", 4)
        train.loadDataFromCatalog(TRAIN_PATH+"tulip/*.jpg", 5)
        
        train.setLogDir(LOG_PATH)
        loss = train.loss
        accuracy = train.accuracy
    
    
    tf.summary.FileWriter(LOG_PATH,tf.get_default_graph())
    
    with tf.Session() as sess:
        init = (tf.global_variables_initializer(), tf.local_variables_initializer())
        sess.run(init)
        if TRAIN_MODE:
            train.prepare(sess)
            train.run(sess,TRAIN_EPOCHE_COUNT)
    
    print("finish")    
