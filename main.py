import tensorflow as tf
    
import nn as nn
import learning as learning

if __name__ == '__main__':
    
#     TRAIN_PATH = "F:\\WORK\\SCIENCE\\DiserDoctor-Calc\\TenzorFlow\\data\\flowers\\"
    TRAIN_PATH = "c:\\temp\\flowers\\"
    LOG_PATH = "F:\\WORK\\SCIENCE\\DiserDoctor-Calc\\TenzorFlow\\log" 
    
    print("start")    
    
    TRAIN_MODE = True
    
    TRAIN_RATE = 0.4
    # epoche count
    TRAIN_EPOCHE_COUNT = 100
    # size of image batch
    BATCH_SIZE = 32
    # size of image
    imageWidth = 200
    imageHeight = 200
    # count category of results
    CATEGORY_COUNT = 5
    # count channel in input images (RGB)
    CHANNEL_COUNT = 3

    # test image
    #image = mpimg.imread(TRAIN_PATH+"flowers\\sample1.png")
    #print("load file succ image.shape = ", image.shape)

    # input
    inputNN = tf.placeholder(tf.float32, shape=[None,imageWidth,imageHeight,CHANNEL_COUNT])
    
    cnn = nn.CNN(tf, imageWidth,imageHeight,input_channel = CHANNEL_COUNT)
    l1_out = cnn.layer1(inputNN)
    l2_out = cnn.layer2(l1_out)
    l3_out = cnn.layer(3, l2_out,feature_count=128)
    fc1_out = cnn.fullyconn(l3_out, 1024)
    drop_out = cnn.dropout(fc1_out)
    #output
    output = cnn.out(drop_out, CATEGORY_COUNT)
    
    print(output)
    
    if TRAIN_MODE:
        train = learning.TrainEx(cnn, inputNN, output, batchSize=BATCH_SIZE, train_rate = TRAIN_RATE)
        #train.loadDataFromCatalog(TRAIN_PATH+"!sample\\*.jpg", 1)
        train.loadDataFromCatalog(TRAIN_PATH+"daisy\\*.jpg", 1)
        train.loadDataFromCatalog(TRAIN_PATH+"dandelion\\*.jpg", 2)
        train.loadDataFromCatalog(TRAIN_PATH+"roses\\*.jpg", 3)
        train.loadDataFromCatalog(TRAIN_PATH+"sunflowers\\*.jpg", 4)
        train.loadDataFromCatalog(TRAIN_PATH+"tulips\\*.jpg", 5)
        
        train.setLogDir(LOG_PATH)
        loss = train.loss
        accuracy = train.accuracy
    
    
    tf.summary.FileWriter(LOG_PATH,tf.get_default_graph())
    
    with tf.Session() as sess:
        init = (tf.global_variables_initializer(), tf.local_variables_initializer())
        sess.run(init)
        if TRAIN_MODE:
            train.prepare(sess)
            train.run(sess,TRAIN_EPOCHE_COUNT)
    
    print("finish")    
