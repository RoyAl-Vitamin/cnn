import numpy as np
from datetime import datetime

if __name__ == '__main__':
    start = datetime.now()
    arr = np.arange(12)
    np.random.shuffle(arr)
    print(arr)
    print(arr.shape)
    arr = np.asarray(np.split(arr,2))
    print(arr)
    print(arr.shape)
    
    duration = datetime.now()-start
    print(duration)
    print(divmod(duration.days * 86400 + duration.seconds, 60))
