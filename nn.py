
class CNN:

    def __init__(self, tf, input_width, input_height, input_channel = 3):
        self.tf = tf
        self.input_width = input_width
        self.input_height = input_height
    #   ���������� ������� �� ������� �����������
    #   �� ��������� 3, �.�. ������� RGB
        self.input_channel = input_channel
    
    def weight_variable(self, shape, name="weight"):
        initial = self.tf.truncated_normal(shape, stddev=0.1)
        return self.tf.Variable(initial, name=name)        
        
    def bias_variable(self, shape, name="bias"):
        initial = self.tf.constant(0.1, shape=shape)
        return self.tf.Variable(initial, name=name)
    
    def conv(self, x, w, name=None):
        return self.tf.nn.conv2d(x, w, strides=[1, 1, 1, 1], padding='SAME', name=name)
    
    def max_pool(self, x, name=None):
#       ���� ������ 2x2 � ����� 2  
        return self.tf.nn.max_pool(x, ksize=[1, 2, 2, 1],strides=[1, 2, 2, 1], padding='SAME', name=name)
    
    def layer1(self, image, feature_count = 32):
#       ������ ���� 5x5   
        w_conv1 = self.weight_variable([5, 5, self.input_channel, feature_count], "weight1")
        b_conv1 = self.bias_variable([feature_count], "bias1")
#       -1 ����������, ��� �� ���� ����� �������� ��� ������� ������� ����������� � ���� ������� 3D, ��� �
#       ����� ������������ � ���� 4D ������� (������� TenzorFlow)
        x_image = self.tf.reshape(image, [-1, self.input_width, self.input_height, self.input_channel]) 
#       ���������� ����
        return self.max_pool(self.tf.nn.relu(self.conv(x_image, w_conv1, name="Conv1") + b_conv1), name="Pool1")
        
    def layer2(self, features_in, feature_count = 64):
#       ������ ���� 5x5   
        w_conv1 = self.weight_variable([5, 5, int(features_in.shape[3]), feature_count], "weight2")
        b_conv1 = self.bias_variable([feature_count], "bias2")
#       ���������� ����
        return self.max_pool(self.tf.nn.relu(self.conv(features_in, w_conv1, name="Conv2") + b_conv1),name="Pool2")

    def layer(self, layer_number, features_in, feature_count = 32):
#       ������ ���� 5x5   
        w_conv1 = self.weight_variable([5, 5, int(features_in.shape[3]), feature_count], "weight"+str(layer_number))
        b_conv1 = self.bias_variable([feature_count], "bias"+str(layer_number))
#       ���������� ����
        return self.max_pool(self.tf.nn.relu(self.conv(features_in, w_conv1, name="Conv"+str(layer_number)) + b_conv1),name="Pool"+str(layer_number))

    def layerConv(self, layer_number, features_in, feature_count, feature_size):
        w_conv1 = self.weight_variable([feature_size, feature_size, int(features_in.shape[3]), feature_count], "weight"+str(layer_number))
        b_conv1 = self.bias_variable([feature_count], "bias"+str(layer_number))
        return self.tf.nn.relu(self.conv(features_in, w_conv1, name="Conv"+str(layer_number)) + b_conv1)
    
    def layerPool(self,layer_number, features_in):
        return self.max_pool(features_in,name="Pool"+str(layer_number))
        
    
    def fullyconn(self, features_in, neuronCount):
        w = int(features_in.shape[1])
        h = int(features_in.shape[2])
        featCount = int(features_in.shape[3])
        #flat
        in_flat = self.tf.reshape(features_in, [-1,w*h*featCount])
        return self.tf.layers.dense(inputs=in_flat, units=neuronCount, activation = self.tf.nn.relu, name="fc1")
    
    def dense(self, layer_number, features_in, neuronCount):
        return self.tf.layers.dense(inputs=features_in, units=neuronCount, activation = self.tf.nn.relu, name="fc"+str(layer_number))
        
    def dropout(self,features_in, rate=0.4):
        self.train_mode = self.tf.placeholder(self.tf.bool)
        return self.tf.layers.dropout(inputs=features_in, rate=rate, training = self.train_mode)
     
    def out(self, input, resultCount):
        self.resultCount = resultCount
        return self.tf.layers.dense(inputs=input, units=resultCount, name="out")
    
    def accuracy(self, trueResults, calcResults):
       correct_prediction = self.tf.equal(self.tf.argmax(trueResults,1), self.tf.argmax(calcResults,1))
       return self.tf.reduce_mean(self.tf.cast(correct_prediction, self.tf.float32))
       
       
       