# CNN
### Dataset
For training cnn we are using ImageNET picture. This archive aviable on [this link](https://cloud.mail.ru/public/HP3Z/1qnhetcAp) and include 5 categories.
Also we are download picture in [FloydHub.com](https://www.floydhub.com/royalvitamin/datasets/flower)
### Prepare to train
Install pip or conda, python, Floyd CLI. [See more](https://docs.floydhub.com/guides/basics/install/)
### How to run this project?
Run cnn vgg16:
```sh
$ git clone https://RoyAl-Vitamin@bitbucket.org/RoyAl-Vitamin/cnn.git
$ cd cnn
$ floyd init royalvitamin/flowers
```
You see in dir 2 files: .floydignore and .floydexpt  
continue:
```sh
$ floyd run --gpu --env tensorflow-1.5 --data royalvitamin/datasets/flower/1:/flowers 'python vgg16e.py'
```
### Monitoring
If no error you see message:
```sh
JOB NAME
--------------------------------
royalvitamin/projects/flowers/<NUMBER_OF_TRY>

URL to job: https://www.floydhub.com/royalvitamin/projects/flowers/<NUMBER_OF_TRY>

To view logs enter:
   floyd logs royalvitamin/projects/flowers/<NUMBER_OF_TRY>
```
Go to `https://www.floydhub.com/royalvitamin/projects/flowers/<NUMBER_OF_TRY>` for see log and system metrics
